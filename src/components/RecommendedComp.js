import React, { useState, useEffect } from "react";
import axios from "../api/albums";
import { Dimensions, Image, StyleSheet, Text, View } from "react-native";

const w = Dimensions.get("window").width;
const h = Dimensions.get("window").height;

const RecommendedComp = (props) => {
  const tagsPhotosRecommendedArray = props.tagsPhotosRecommendedArray;
  const [photosObjects, setPhotosObjects] = useState();

  const getPhotosObjectsArray = async () => {
    try {
      const photos = await axios.get("/getPhotos", {
        params: {
          photosIDS: tagsPhotosRecommendedArray,
        },
      });

      setPhotosObjects(photos.data);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getPhotosObjectsArray();
  }, []);

  return (
    <View
      style={{
        backgroundColor: "#FFF",
        borderRadius: 50,
        flex: 1,
        alignItems: "center",
      }}
    >
      <Text
        style={{
          color: "#000",
          fontSize: 18,
          marginTop: 30,
        }}
      >
        Photos & Videos
      </Text>
      <Text
        style={{
          color: "#003f5c",
          fontSize: 18,
        }}
      >
        {tagsPhotosRecommendedArray.length} shots
      </Text>

      {photosObjects != null ? (
        <View
          style={{
            flexDirection: "row",
            marginTop: 20,
          }}
        >
          {photosObjects[0] != null ? (
            photosObjects[0].length > 0 ? (
              <Image
                source={{
                  uri: photosObjects[0][0].url.toString(),
                }}
                style={{
                  height: 280,
                  width: 150,
                  borderTopLeftRadius: 40,
                  marginRight: 16,
                  borderBottomLeftRadius: 40,
                }}
              />
            ) : null
          ) : null}

          <View>
            {photosObjects[1] != null ? (
              photosObjects[1].length > 0 ? (
                <Image
                  source={{
                    uri: photosObjects[1][0].url.toString(),
                  }}
                  style={{ borderTopRightRadius: 40, height: 132, width: 120 }}
                />
              ) : null
            ) : null}

            {photosObjects[2] != null ? (
              photosObjects[2].length > 0 ? (
                <Image
                  source={{
                    uri: photosObjects[2][0].url.toString(),
                  }}
                  style={{
                    marginTop: 16,
                    borderBottomRightRadius: 40,
                    height: 132,
                    width: 120,
                  }}
                />
              ) : null
            ) : null}
          </View>
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({});

export default RecommendedComp;
