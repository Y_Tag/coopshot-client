/** @format */

import React, { useEffect, useState, useContext } from 'react'
import { Text, StyleSheet, View, FlatList, Image } from 'react-native'
import axios from '../api/albums'
import { Button } from 'react-native'
import { updatePartners } from '../helpers/helpersFunc'
import Loader from '../components/Loader'
import FlatListIndicator from '../components/FlatListIndicator'
import { Context } from '../context/AuthContext'
import * as Contacts from 'expo-contacts'
import { TextInput, ScrollView } from 'react-native-gesture-handler'
import { LinearGradient } from 'expo-linear-gradient'
import { ListItem, SearchBar } from 'react-native-elements'

const AddPatnersScreen = ({ navigation }) => {
  const justNum = 1

  const { state } = useContext(Context);
  const [search, setSearch] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [originalUsers, setOriginalUsers] = useState([]);
  const [partners, setPartners] = useState([]);
  const albumID = navigation.getParam("id");

  useEffect(() => {
    const fetchData = async () => {
      var resultAllUsers = await axios.get('/user/all')

      const resultPartners = await axios.get('/album/partners', {
        params: { albumId: navigation.getParam('id') },
      })

      const { status } = await Contacts.requestPermissionsAsync()
      if (status === 'granted') var { data } = await Contacts.getContactsAsync()

      var devicePhoneNumbers = []
      var phonenumber = ''
      // filter the return contacts from the expo-Contacts
      for (let i = 0; i < data.length; i++) {
        phonenumber = ''
        //get only the contacts with a phone number
        if (data[i].phoneNumbers !== undefined) {
          // verify the contacts has phone number
          if (data[i].phoneNumbers[0].digits !== undefined)
            // for IPhone
            phonenumber = data[i].phoneNumbers[0].digits
          else if (data[i].phoneNumbers[0].number !== undefined)
            // for Android
            phonenumber = data[i].phoneNumbers[0].number
          // remove the +972 from phone number
          if (phonenumber.includes('+972'))
            phonenumber = phonenumber.replace('+972', '0')
          if (phonenumber.includes('-'))
            // remove - from phone number
            phonenumber = phonenumber.replaceAll('-', '')
          devicePhoneNumbers.push(phonenumber)
        }
      }

      var filteredUsers = []
      // filter the return contacts from the server
      for (let j = 0; j < resultAllUsers.data.length; j++) {
        var p
        if (resultAllUsers.data[j].phone != null) {
          if (resultAllUsers.data[j].phone.includes('+972')) {
            // if have thw phone number includs +972
            p = resultAllUsers.data[j].phone.replace('+972', '0')
          } else {
            p = resultAllUsers.data[j].phone
          }
          if (devicePhoneNumbers.includes(p)) {
            // if the phone number is in the filtered numbers from expo-Contacts
            filteredUsers.push(resultAllUsers.data[j])
          }
        }
      }

      setSearchResults(filteredUsers);
      setOriginalUsers(filteredUsers);
      setPartners(resultPartners.data);
    };

    fetchData()
  }, [justNum])

  const searchFilterFunction = (text) => {
    // Check if searched text is not blank
    if (!text) {
      // Inserted text is not blank
      // Filter the Users
      // Update Users2
      setSearchResults(originalUsers);
      setSearch(text);
      return;
    }
    const newData = originalUsers.filter(function (item) {
      const itemData = item.email ? item.email.toUpperCase() : "".toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    setSearchResults(newData);
    setSearch(text);
  };

  const ItemView = ({ item }) => {
    return (
      // Flat List Item
      <View>
        <View style={styles.row}>
          <Text style={styles.name}>{item.email} </Text>

          <FlatListIndicator
            isPartner={partners.partners.includes(item._id)}
            albumIdSent={albumID}
            userIdSent={item._id}
          />
        </View>
      </View>
    )
  }

  const ItemSeparatorView = () => {
    return (
      // Flat List Item Separator
      <View
        style={{
          height: 0.5,
          width: '100%',
          backgroundColor: '#C8C8C8',
        }}
      />
    )
  }

  if (!partners.partners)
    // loader until all the data will return from the api
    return (
      <View>
        <Loader />
      </View>
    )
  return (
    <ScrollView showsHorizontalScrollIndicator={false} style={{ height: 400 }}>
      <View
        style={{
          backgroundColor: '#FFF',
          flex: 1,
        }}
      >
        <View
          style={{
            backgroundColor: '#003f5c',
            height: 170,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            paddingHorizontal: 20,
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 25,
              width: '100%',
            }}
          >
            <View style={{ marginTop: 50, width: '70%' }}>
              <Text
                style={{
                  fontSize: 28,
                  color: '#FFF',
                  fontWeight: 'bold',
                }}
              >
                Album Partners
              </Text>
            </View>

            <View style={{ marginTop: 20, width: '30%' }}>
              <Image
                source={require('../../assets/partnersImage.png')}
                style={{ height: 80, width: 80 }}
              />
            </View>
          </View>
        </View>
        <LinearGradient
          colors={['rgba(0,63,92,0.4)', 'transparent']}
          style={{
            left: 0,
            right: 0,
            height: 90,
            marginTop: -45,
          }}
        >
          <View
            style={{
              backgroundColor: '#FFF',
              paddingVertical: 8,
              paddingHorizontal: 20,
              marginHorizontal: 20,
              borderRadius: 15,
              marginTop: 25,
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <TextInput
              placeholder="Search"
              placeholderTextColor="#003f5c"
              style={{
                fontWeight: 'bold',
                fontSize: 18,
                width: 260,
              }}
              onChangeText={(text) => searchFilterFunction(text)}
              value={search}
            />
          </View>
        </LinearGradient>

        <View style={styles.container}>
          {/* Button For sending the array to the server for updating the partners */}

          <View>
            <FlatList
              data={searchResults.filter(function (user) {
                return user._id !== state.userID;
              })}
              keyExtractor={(itemUser) => itemUser._id}
              renderItem={ItemView}
              ItemSeparatorComponent={ItemSeparatorView}
            />
          </View>
        </View>
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderColor: 'grey',
    padding: 15,
  },
  name: {
    fontSize: 18,
  },
  container: {
    flex: 1,
  },
  list: {
    paddingHorizontal: 10,
  },
  listContainer: {
    alignItems: 'center',
  },
  separator: {
    marginTop: 10,
  },
})

export default AddPatnersScreen
