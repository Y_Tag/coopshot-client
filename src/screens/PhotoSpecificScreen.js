import React, { useContext } from "react";
import { Text, StyleSheet, View, TextInput, Button, Image } from "react-native";
import { Context as AlbumContext } from "../context/AlbumContext";
import { TouchableOpacity } from "react-native-gesture-handler";
import axios from "../api/albums";
import FloatingPhoto from "../components/FloatingPhoto";

const PhotoSpecificScreen = (props) => {
  const { navigation } = props;
  const { state } = useContext(AlbumContext);
  const imageObj = navigation.getParam("imageObj");
  const photosObjectAlbum = navigation.getParam("photosObjectAlbum");

  return (
    <View>
      <Image style={styles.image} source={{ uri: imageObj.url }}></Image>
      <FloatingPhoto
        nav={navigation}
        image={imageObj}
        photosObjectAlbum={photosObjectAlbum}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%",
    resizeMode: 'contain',
    backgroundColor: '#E6E6E6'
  },
  // row: {
  //     flexDirection: 'row',
  //     justifyContent: 'space-around',
  //     alignItems: 'flex-start',
  //     marginTop: 20,

  // }
});

export default PhotoSpecificScreen;
