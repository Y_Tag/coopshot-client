import React from "react";
import { View, StyleSheet, Platform } from "react-native";
import { AssetsSelector } from "expo-images-picker";
import { Ionicons } from "@expo/vector-icons";
import { UploadFromGallery } from "../api/albums";
import Constants from "expo-constants";

const MultiPickerScreen = ({ navigation }) => {
  const maxSelections = navigation.getParam("maxSelections");
  const path = navigation.getParam("path");
  const onDone = (data) => {
    if (data.length > 0) {
      const albumId = navigation.getParam("albumId");

      if (albumId) {
        var photosUrl = data.map((photo) => {
          return {
            uri: photo.uri,
            filename: photo.filename,
          };
        });
        UploadFromGallery(photosUrl, albumId, "true");
      } else {
        // the screen got here from the profile screen
        navigation.state.params.onBack({
          uri: data[0].uri,
          filename: data[0].filename,
        });
      }
    }

    if (path === "createNewAlbum") {
      navigation.replace("Albums");
    } else {
      navigation.goBack();
    }

    // navigation.navigate('SingleAlbum', { albumId }); // ****Need to pass here the id of the album again
  };

  return (
    <View style={styles.container}>
      <AssetsSelector
        options={{
          assetsType: ["photo"], // can add "video" -> will see the photos and videos
          maxSelections: maxSelections,
          margin: 2,
          portraitCols: 4,
          landscapeCols: 5,
          widgetWidth: 100,
          widgetBgColor: "white",
          videoIcon: {
            Component: Ionicons,
            iconName: "ios-videocam",
            color: "tomato",
            size: 20,
          },
          selectedIcon: {
            Component: Ionicons,
            iconName: "ios-checkmark-circle-outline",
            color: "white",
            bg: "#0eb14970",
            size: 26,
          },
          defaultTopNavigator: {
            continueText: "Finish",
            selectedText: "Selected",
            buttonStyle: _buttonStyle,
            buttonTextStyle: _textStyle,
            backFunction: () => {},
            doneFunction: (data) => onDone(data),
          },
          spinnerColor: "black",
          onError: () => {},
          noAssets: (data) => <View></View>,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
    paddingTop: Platform.OS === "ios" ? Constants.statusBarHeight + 10 : 10,
    flex: 1,
    // height: '75%',
  },
});
const _textStyle = {
  color: "blue",
};
const _buttonStyle = {
  backgroundColor: "white",
  borderRadius: 18,
};

export default MultiPickerScreen;
