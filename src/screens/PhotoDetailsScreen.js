import React, { useEffect, useState } from "react";
import { Text, StyleSheet, View, Switch, AsyncStorage } from "react-native";
import {
  MaterialIcons,
  Ionicons,
  AntDesign,
  Entypo,
  FontAwesome,
} from "@expo/vector-icons";
import { SafeAreaView } from "react-native-safe-area-context";
import Map from "../components/Map";
import axios from "../api/albums";
import Loader from "../components/Loader";

const PhotoDetailsScreen = (props) => {
  const { navigation } = props;
  const imageObj = navigation.getParam("imageObj");
  const photosObjectAlbum = navigation.getParam("photosObjectAlbum");

  var cords = imageObj.coords;
  const [isEnabled, setIsEnabled] = useState(imageObj.private);
  const toggleSwitch = () => {
    setIsEnabled((previousState) => !previousState);
    axios.put("/photo/private", {
      photoID: imageObj._id,
      photoPrivate: !isEnabled,
    });
  };
  const [email, setEmail] = useState("");
  const [UserId, setUserId] = useState("");

  var newDate = new Date(imageObj.timestamp);
  var date =
    newDate.getHours() +
    ":" +
    newDate.getMinutes() +
    ", " +
    newDate.toDateString();
  var authorID = imageObj.author;

  const searchEmail = async () => {
    const authorName = await axios.get("/user/findemailbyid", {
      params: {
        userId: authorID,
      },
    });

    setEmail(authorName.data.email);
  };

  useEffect(() => {
    const getUserId = async () => {
      const currentUser = await AsyncStorage.getItem("retUserId");
      setUserId(currentUser);
    };
    getUserId();
    searchEmail();
  }, []);

  if (!email || !setUserId)
    // loader until all the data will return from the api
    return (
      <View>
        <Loader />
      </View>
    );

  //                           -----------^^^^^^^------------

  return (
    <View style={{ backgroundColor: "#003f5c" }}>
      <View style={styles.column}>
        <Text
          style={{
            marginTop: 10,
            fontSize: 32,
            color: "white",
            marginLeft: 100,
            fontWeight: "700",
          }}
        >
          Photo Details
        </Text>
        <View
          style={{
            backgroundColor: "#FFF",
            paddingVertical: 8,
            paddingHorizontal: 20,
            marginHorizontal: 20,
            borderRadius: 15,
            marginTop: 25,
          }}
        >
          {UserId === authorID ? (
            <View style={styles.row}>
              <View style={{ width: "80%" }}>
                <View style={styles.row}>
                  <FontAwesome
                    name="user-secret"
                    size={35}
                    color="#003f5c"
                    style={{ marginRight: 10 }}
                  />
                  <Text>Private</Text>
                </View>
              </View>

              <View style={{ width: "20%" }}>
                <Switch
                  style={styles.toggleSwitch}
                  trackColor={{ false: "#767577", true: "#81b0ff" }}
                  thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={toggleSwitch}
                  value={isEnabled}
                />
              </View>
            </View>
          ) : null}
          <View style={styles.row}>
            <MaterialIcons
              name="date-range"
              size={35}
              color="#003f5c"
              style={{ marginRight: 10 }}
            />
            <Text style={{ width: "70%" }}>{date}</Text>
          </View>

          <View style={styles.row}>
            <AntDesign
              name="picture"
              size={35}
              color="#003f5c"
              style={{ marginRight: 10 }}
            />
            <Text style={{ width: "90%" }}>{imageObj.url}</Text>
          </View>
          <View style={styles.row}>
            <Ionicons
              name="person"
              size={35}
              color="#003f5c"
              style={{ marginRight: 10 }}
            />
            <Text>{email}</Text>
          </View>
        </View>
      </View>
      <SafeAreaView>
        <Map
          latitude={cords.latitude}
          longitude={cords.longitude}
          imageURI={imageObj.url}
          imageObj={imageObj}
          photosObjectAlbum={photosObjectAlbum}
          nav={navigation}
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  column: {
    flexDirection: "column",
    marginTop: 50,
    justifyContent: "space-around",
  },
  row: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 5,
    alignItems: "center",
  },
  toggleSwitch: {
    backgroundColor: "transparent",
  },

  Container: {
    marginTop: 25,
    marginBottom: 10,
    marginLeft: 5,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});

export default PhotoDetailsScreen;
