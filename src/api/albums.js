/** @format */
import axios from "axios";
import { AsyncStorage } from "react-native";
import { NGROK_URL } from "@env";
import { Platform } from "react-native";
import { getCurrentPositionAsync } from "expo-location";
import shorthash from "shorthash";
import * as FileSystem from "expo-file-system";
url = "https://coopshot.herokuapp.com";

const instance = axios.create({
  baseURL: url, //This URL needs to be updated every 8 hours. ngrok http <portnumber>
});

instance.interceptors.request.use(
  async (config) => {
    const token = await AsyncStorage.getItem("token");

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);

export default instance;

const UploadPhotos = async (body, albumID) => {
  //Form Data - Upload the image to server
  const token = await AsyncStorage.getItem("token");
  let glocation = await getCurrentPositionAsync({});

  body.append("Content-Type", "image/png");

  body.append("timestamp", Date.now());
  body.append("alt", glocation.coords.altitude);
  body.append("long", glocation.coords.longitude);
  body.append("lat", glocation.coords.latitude);

  fetch(`${url}/api/upload`, {
    method: "POST",
    headers: {
      "Content-Type": "multipart/form-data",
      otherHeader: "foo",
      Authorization: `Bearer ${token}`,
      //For Now I gave this an "Basic Authorization" ->Need to change to real auth from the specific user
    },
    body: body,
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw response;
      }
    })
    .then((responseJson) => {
      console.log("Upload success!");

      for (const photoId of responseJson.data) {
        //for each photo add the relevant tags
        instance
          .put("/photo/settags", {
            photoId,
          })
          .catch((error) => {
            const photo = error.response.data.photo;
            // means he settags request , didn't work
            console.error("set tags to photo failed - photo will be deleted");
            instance
              .post("/api/deletephoto", {
                image_id: photoId,
                albumID,
                public_id: photo.public_id,
              })
              .catch((error) => {
                console.error(error.message);
              });
          });
      }
    })
    .catch((error) => {
      error.json().then((body) => {
        console.error(body.message);
      });
    })
    .done();

  // .then((res) => checkStatus(res))
  // .then((res) => res.json())
};

const UploadFromCamera = async (photo, albumId, isPrivate) => {
  let body = new FormData();

  var splitted = photo.uri.split("/");
  const fileName = splitted[splitted.length - 1];
  body.append("photos", {
    uri: Platform.OS === "ios" ? photo.uri.replace("file://", "") : photo.uri,
    name: fileName,
    type: `image/${fileName.split(".")[1]}`, // split the file name for getting extension
  });

  body.append("albumId", albumId);
  body.append("private", isPrivate);
  UploadPhotos(body, albumId);
};

const UploadFromGallery = async (photos, albumId, isPrivate) => {
  let body = new FormData();

  for (let photo of photos) {
    body.append("photos", {
      uri: photo.uri,
      name: photo.filename,
      type: `image/${photo.filename.split(".")[1]}`, // split the file name for getting extension
    });
  }

  body.append("albumId", albumId);
  body.append("private", isPrivate);

  UploadPhotos(body, albumId);
};

const UploadProfilePic = async (name, email, phone, profilepic) => {
  let body = new FormData();
  body.append("name", name);
  body.append("email", email);
  body.append("phone", phone);

  // add the photo to the body of request
  body.append("profilepic", {
    uri: profilepic.uri,
    name: profilepic.filename,
    type: `image/PNG`, // split the file name for getting extension
  });

  const token = await AsyncStorage.getItem("token");
  const currentID = await AsyncStorage.getItem("retUserId");

  body.append("Content-Type", "image/png");

  fetch(`${url}/user/edituser`, {
    method: "PUT",
    headers: {
      "Content-Type": "multipart/form-data",
      otherHeader: "foo",
      Authorization: `Bearer ${token}`,
      //For Now I gave this an "Basic Authorization" ->Need to change to real auth from the specific user
    },
    body: body,
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw response;
      }
    })
    .then(async (responseJson) => {
      //update the value
      //TODO: Note that the password returns as hashed !!!
      try {
        responseJson.image = await checkImageInCache(responseJson.imageUrl);
        await AsyncStorage.setItem(
          currentID + " user details",
          JSON.stringify(responseJson)
        );
      } catch (err) {
        console.log(err);
      }
    })
    .catch((error) => {
      error.json().then((body) => {
        console.error(body.message);
      });
    })
    .done();
};

const checkImageInCache = async (element) => {
  const name = shorthash.unique(element);
  const path = `${FileSystem.cacheDirectory}${name}`;
  const image = await FileSystem.getInfoAsync(path);
  if (image.exists) {
    return image.uri;
  } else {
    const newImage = await FileSystem.downloadAsync(element, path);
    return newImage.uri;
  }
};

export { UploadFromCamera, UploadFromGallery, UploadProfilePic };
