/** @format */

import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import SignUpScreen from './src/screens/SignUpScreen';
import SignInScreen from './src/screens/SignInScreen';
import AlbumSingleScreen from './src/screens/AlbumSingleScreen';
import AlbumsScreen from './src/screens/AlbumsScreen';
import AlbumCreationScreen from './src/screens/AlbumCreationScreen';
import CameraScreen from './src/screens/CameraScreen';
import { Provider as AuthProvider } from './src/context/AuthContext';
import { Provider as AlbumProvider } from './src/context/AlbumContext';
import { Provider as GlobalProvider } from './src/context/GlobalContext';
import { setNavigator } from './src/navigationRef';
import ResolveAuthScreen from './src/screens/ResolveAuthScreen';
import AlbumNameEdit from './src/screens/AlbumNameEdit';
import AlbumDescEdit from './src/screens/AlbumDescEdit';
import ToUploadScreen from './src/screens/ToUploadScreen';
import FloatingButtonComp from './src/components/FloatinButtonComp';
import PhotoSpecificScreen from './src/screens/PhotoSpecificScreen';
import PhotoDetailsScreen from './src/screens/PhotoDetailsScreen';
import AddPatnersScreen from './src/screens/AddPartnersScreen';
import HomeScreen from './src/screens/HomeScreen';
import MultiPickerScreen from './src/screens/MultiPickerScreen';
import RecommendationScreen from './src/screens/RecommendationScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import FloatingPhoto from './src/components/FloatingPhoto';

const switchNavigator = createSwitchNavigator(
  {
    ResolveAuthScreen,
    loginFlow: createStackNavigator({
      SignIn: SignInScreen,
      SignUp: SignUpScreen,
    }),
    mainFlow: createStackNavigator(
      {
        Home: HomeScreen,
        Albums: AlbumsScreen,
        SingleAlbum: AlbumSingleScreen,
        Recommendation: RecommendationScreen,
        AlbumCreation: AlbumCreationScreen,
        ToUpload: ToUploadScreen,
        FloatingScreen: FloatingButtonComp,
        Floating: FloatingPhoto,
        SpecificPhoto: PhotoSpecificScreen,
        PhotoDetails: PhotoDetailsScreen,
        AddPartnerToAlbum: AddPatnersScreen,
        Camera: {
          screen: CameraScreen,
          navigationOptions: {
            headerShown: false, //this will hide the header
          },
        },
        EditAlbumName: AlbumNameEdit,
        EditAlbumDesc: AlbumDescEdit,
        MultiPicker: MultiPickerScreen,
        Profile: ProfileScreen,
      },
      {
        headerMode: 'none',
        headerShown: false,
      }
    ),
  },
  {
    initialRouteName: 'ResolveAuthScreen',
  }
);

const App = createAppContainer(switchNavigator);

export default () => {
  return (
    <AuthProvider>
      <AlbumProvider>
        <GlobalProvider>
          <App
            ref={(navigator) => {
              setNavigator(navigator);
            }}
          />
        </GlobalProvider>
      </AlbumProvider>
    </AuthProvider>
  );
};
